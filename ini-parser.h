/*
 * =====================================================================================
 *
 *       Filename:  ini-parser.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  06/03/24 06:38:16 PM IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kashyap Ekbote (Krash), kashyap.ekbote@gmail.com
 *        Company:  no
 *
 * =====================================================================================
 */

#include <memory>
#include <vector>

#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree_fwd.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ini_parser.hpp>

class INIParser
{
public:
  INIParser(const std::string& file_path) : m_file_path(file_path)
  {
    boost::property_tree::ini_parser::read_ini(m_file_path, m_data);
  }
  ~INIParser()
  {
    write();
  }

  template<typename T>
  T get_value(const std::string& key)
  {
    return m_data.get<T>(key);
  }
  template<typename T>
  void set_value(const std::string& key, const T& value)
  {
    m_is_file_changed = true;
    m_data.put(key, value);
  }

  template<typename T>
  std::vector<T> get_vec(const std::string& key)
  {
    std::vector<T> result;
    std::stringstream ss(m_data.get<T>(key));
    std::string item;
    while (std::getline(ss, item, ',')) {
      result.push_back(boost::lexical_cast<T>(item));
    }
    return result;
  }

private:
  void write()
  {
    if (m_is_file_changed) {
      boost::property_tree::ini_parser::write_ini(m_file_path, m_data);
    }
  }

private:
  const std::string& m_file_path;

  bool m_is_file_changed = false;
  boost::property_tree::ptree m_data;
};
